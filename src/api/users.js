import axios from "axios";

export const getUsers = () => {
  return axios.get(`${process.env.REACT_APP_BASE_URL}/users`, {
    params: {
      limit: 10
    }
  });
};

export const createUser = user => {
  return axios.post(`${process.env.REACT_APP_BASE_URL}/users`, user);
};

export const editUserPut = (userId, user) => {
  return axios.put(`${process.env.REACT_APP_BASE_URL}/users/${userId}`, user);
};

export const deleteUser = userId => {
  return axios.delete(`${process.env.REACT_APP_BASE_URL}/users/${userId}`);
};
