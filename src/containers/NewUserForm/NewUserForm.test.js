import React from "react";
import { configure, shallow, mount } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import NewUserForm from "./NewUserForm";
import Input from "../../UI/Input/Input";

configure({ adapter: new Adapter() });

describe("<NewUserForm />", () => {
  let wrapper;
  let wrappInput;
  beforeEach(() => {
    wrapper = shallow(<NewUserForm />);
    wrappInput = shallow(<Input />);
  });
  it("should be input field", () => {
    wrappInput.setProps({ element: "input" });
    expect(wrappInput.find("input")).toHaveLength(1);
  });
  it("should be textarea field", () => {
    wrappInput.setProps({ element: "textarea" });
    expect(wrappInput.find("textarea")).toHaveLength(1);
  });
  it("should be select field", () => {
    wrappInput.setProps({ element: "select" });
    expect(wrappInput.find("select")).toHaveLength(1);
  });
});
