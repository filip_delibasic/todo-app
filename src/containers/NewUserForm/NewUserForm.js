import React, { Component } from "react";
import Input from "../../UI/Input/Input";
import JSON from "../../content.json";
import "./NewUserForm.scss";
import Button from "../../UI/Button/Button";

class NewUserForm extends Component {
  state = {
    userData: {
      firstName: {
        firstName: "",
        touch: false,
        errorMessage: ""
      },
      lastName: {
        lastName: "",
        touch: false,
        errorMessage: ""
      },
      email: {
        email: "",
        touch: false,
        errorMessage: ""
      },
      age: {
        age: "",
        touch: false,
        errorMessage: ""
      },
      company: {
        company: "",
        touch: false,
        errorMessage: ""
      },
      description: {
        description: "",
        touch: true,
        errorMessage: ""
      },
      position: {
        position: "",
        touch: false,
        errorMessage: ""
      }
    },
    form: JSON
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevState.userData !== this.state.userData) {
    }
  }

  handleInputChange = e => {
    const { value, name } = e.target;
    const updateUserData = { ...this.state.userData };
    const updateUserDataElement = updateUserData[name];
    updateUserDataElement[name] = value;
    if (value) {
      updateUserDataElement.errorMessage = "";
    }
    updateUserData[name] = updateUserDataElement;
    this.setState({ userData: updateUserData });
  };

  handleOnFocus = e => {
    const { name } = e.target;
    const updateUserData = { ...this.state.userData };
    const updateUserDataElement = updateUserData[name];
    updateUserDataElement.touch = true;
    this.setState({ userData: updateUserData });
  };

  handleOnBlur = e => {
    const { name } = e.target;
    const updateUserData = { ...this.state.userData };
    const updateUserDataElement = updateUserData[name];
    if (
      updateUserDataElement.touch &&
      !updateUserDataElement[name] &&
      !updateUserDataElement.errorMessage
    ) {
      updateUserDataElement.errorMessage = `${name} is required!`;
    }
    updateUserData[name] = updateUserDataElement;
    this.setState({ userData: updateUserData });
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.onSubmit(this.state.userData);
  };

  handleOnUpdate = e => {
    e.preventDefault();
    this.props.onSave(this.props.userId, this.state.userData);
  };

  renderInput = () => {
    const { form, userData } = this.state;
    const content = Object.keys(form.userData).map(item => {
      return (
        <Input
          key={item}
          element={item.element}
          touch={userData[item].touch}
          value={userData[item][item]}
          options={item.options}
          {...form.userData[item]}
          onChange={this.handleInputChange}
          onFocus={this.handleOnFocus}
          onBlur={this.handleOnBlur}
          errmessage={userData[item].errorMessage}
        />
      );
    });
    return content;
  };

  handleValidForm = () => {
    const { userData } = this.state;
    const requiredFields = Object.keys(userData).filter(
      item => item !== "description"
    );
    const errMessageFields = requiredFields.map(
      item => userData[item].errorMessage
    );
    const touchedArray = Object.keys(userData).map(
      item => userData[item].touch
    );
    const errMessage = errMessageFields.every(item => item === "");
    const isAllTouched = touchedArray.every(item => item !== false);
    if (errMessage && isAllTouched) {
      return true;
    }
    return false;
  };

  render() {
    const isValidForm = this.handleValidForm();
    const { closeModal, addUser } = this.props;
    return (
      <form onSubmit={addUser ? this.handleSubmit : this.handleOnUpdate}>
        {this.renderInput()}
        <Button
          className={isValidForm ? "create-btn" : "create-btn disabled"}
          disabled={!isValidForm}
        >
          {addUser ? "Create User" : "Update user"}
        </Button>
        <Button className="close-btn" handleClick={closeModal}>
          <i className="material-icons">close</i>
        </Button>
      </form>
    );
  }
}

export default NewUserForm;
