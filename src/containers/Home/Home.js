import React, { Component } from "react";
import { connect } from "react-redux";
import {
  getUsersRequest,
  createUserRequest,
  deleteUserRequest,
  editUserPutRequest
} from "../../store/actions/users";
import { loaderAction } from "../../store/actions/loader";
import NewUserForm from "../NewUserForm/NewUserForm";
import UsersList from "../../components/UsersList/UsersList";
import Button from "../../UI/Button/Button";
import Modal from "../../UI/Modal/Modal";
import Backdrop from "../../UI/Backdrop/Backdrop";
import "./Home.scss";
import ConfirmDelete from "../../components/ConfirmDelete/ConfirmDelete";
import Loader from "../../UI/Loader/Loader";
import ErrorHandler from "../../UI/ErrorHandler/ErrorHandler";

class Home extends Component {
  state = {
    addUser: false,
    editUser: false,
    deleteUser: false,
    id: 0
  };

  componentDidMount() {
    this.props.getUsers();
  }

  // componentDidUpdate(prevProps) {
  //   if (prevProps.users !== this.props.users) {
  //     this.props.users.map((item, index) =>
  //       localStorage.setItem(index, JSON.stringify(item))
  //     );
  //   }
  // }

  onSubmit = userData => {
    this.props.createUser(userData);
    this.setState({ addUser: false });
  };

  openModal = () => {
    this.setState({ addUser: true });
  };

  closeModal = () => {
    this.setState({ addUser: false, editUser: false, deleteUser: false });
  };

  handleEditUser = userId => {
    this.setState({ editUser: true, id: userId });
  };

  handleEditOnSave = (userId, user) => {
    this.props.editUserPut(userId, user);
    this.setState({ editUser: false });
  };

  openDeleteModal = userId => {
    this.setState({ deleteUser: true, id: userId });
  };

  handleDeleteUser = () => {
    this.props.deleteUser(this.state.id);
    this.setState({ deleteUser: false });
  };

  render() {
    const { users, loaderState, errorUser } = this.props;
    const { addUser, editUser, deleteUser, loader, id } = this.state;
    let backdrop = null;
    let modal = null;
    let loading = null;
    let errorHandler = null;
    if (addUser || editUser) {
      backdrop = <Backdrop />;
      modal = (
        <Modal loader={loader}>
          <NewUserForm
            loader={loader}
            onSubmit={this.onSubmit}
            onSave={this.handleEditOnSave}
            closeModal={this.closeModal}
            addUser={addUser}
            userId={id}
          />
        </Modal>
      );
    }
    if (deleteUser) {
      backdrop = <Backdrop />;
      modal = (
        <Modal>
          <ConfirmDelete
            closeModal={this.closeModal}
            deleteUser={this.handleDeleteUser}
          />
        </Modal>
      );
    }
    if (loaderState) {
      backdrop = <Backdrop />;
      loading = (
        <Modal loader={loaderState}>
          <Loader />
        </Modal>
      );
    }
    if (errorUser) {
      errorHandler = <ErrorHandler error={errorUser} />;
    }

    return (
      <div className="container">
        {modal}
        {backdrop}
        {loading}
        {errorHandler}
        <UsersList
          users={users}
          editUser={this.handleEditUser}
          deleteModal={this.openDeleteModal}
        />
        {process.env.REACT_APP_NAME}
        <Button className="add-user-btn" handleClick={this.openModal}>
          <i className="material-icons">add</i>
        </Button>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    users: state.users,
    user: state.user,
    loaderState: state.loader,
    errorUser: state.errorUser
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getUsers: () => dispatch(getUsersRequest()),
    createUser: user => dispatch(createUserRequest(user)),
    deleteUser: userId => dispatch(deleteUserRequest(userId)),
    editUserPut: (userId, user) => dispatch(editUserPutRequest(userId, user)),
    loaderAction: bool => dispatch(loaderAction(bool))
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
