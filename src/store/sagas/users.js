import {
  takeEvery,
  takeLatest,
  take,
  call,
  put,
  fork,
  delay
} from "redux-saga/effects";
import * as actions from "../actions/users";
import * as actionsLoad from "../actions/loader";
import * as api from "../../api/users";

function* getUsers() {
  try {
    const result = yield call(api.getUsers);
    yield put(actions.getUsersSuccess(result.data));
  } catch (e) {
    yield put(
      actions.errorUsers("Error is occured when trying to get the users")
    );
  }
}

function* createUser(action) {
  try {
    yield call(api.createUser, action.payload);
    yield put(actionsLoad.loaderAction(true));
    yield delay(1000);
    yield call(getUsers);
    yield put(actionsLoad.loaderAction(false));
  } catch (e) {
    yield put(
      actions.errorUsers("Error is occured when trying to create the user")
    );
  }
}

function* editUserPut(action) {
  try {
    yield call(api.editUserPut, action.userId, action.user);
    yield put(actionsLoad.loaderAction(true));
    yield delay(1000);
    yield call(getUsers);
    yield put(actionsLoad.loaderAction(false));
  } catch (e) {
    yield put(
      actions.errorUsers("Error is occured when trying to update the user")
    );
  }
}

function* deleteUser(userId) {
  try {
    yield call(api.deleteUser, userId);
    yield put(actionsLoad.loaderAction(true));
    yield delay(1000);
    yield call(getUsers);
    yield put(actionsLoad.loaderAction(false));
  } catch (e) {
    yield put(
      actions.errorUsers("Error is occured when trying to delete the user")
    );
  }
}

function* watchGetUsersRequest() {
  yield takeEvery(actions.Types.GET_USERS_REQUEST, getUsers);
}

function* watchCreateUserRequest() {
  yield takeLatest(actions.Types.CREATE_USER_REQUEST, createUser);
}

function* watchEditUserPutRequest() {
  while (true) {
    const action = yield take(actions.Types.EDIT_USER_PUT_REQUEST, editUserPut);
    yield call(editUserPut, action.payload);
  }
}

function* watchDeleteUserRequest() {
  while (true) {
    const action = yield take(actions.Types.DELETE_USER_REQUEST);
    yield call(deleteUser, action.payload);
  }
}
const usersSagas = [
  fork(watchGetUsersRequest),
  fork(watchCreateUserRequest),
  fork(watchDeleteUserRequest),
  fork(watchEditUserPutRequest)
];

export default usersSagas;
