export const Types = {
  LOADER: "LOADER"
};

export const loaderAction = bool => {
  return {
    type: Types.LOADER,
    payload: bool
  };
};
