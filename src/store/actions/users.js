export const Types = {
  GET_USERS_REQUEST: "GET_USERS_REQUEST",
  GET_USERS_SUCCESS: "GET_USERS_SUCCESS",
  CREATE_USER_REQUEST: "CREATE_USER_REQUEST",
  CREATE_USER_SUCCESS: "CREATE_USER_SUCCESS",
  EDIT_USER_GET_REQUEST: "EDIT_USER_GET_REQUEST",
  EDIT_USER_PUT_REQUEST: "EDIT_USER_PUT_REQUEST",
  DELETE_USER_REQUEST: "DELETE_USER_REQUEST",
  ERROR_USERS: "ERROR_USERS"
};

export const getUsersRequest = () => {
  return {
    type: Types.GET_USERS_REQUEST
  };
};

export const getUsersSuccess = users => {
  return {
    type: Types.GET_USERS_SUCCESS,
    payload: users
  };
};

export const createUserRequest = user => {
  return {
    type: Types.CREATE_USER_REQUEST,
    payload: user
  };
};

export const createUserSuccess = () => {
  return {
    type: Types.CREATE_USER_SUCCESS
  };
};

export const editUserPutRequest = (userId, user) => {
  return {
    type: Types.EDIT_USER_PUT_REQUEST,
    payload: { userId, user }
  };
};

export const deleteUserRequest = userId => {
  return {
    type: Types.DELETE_USER_REQUEST,
    payload: userId
  };
};

export const errorUsers = error => {
  return {
    type: Types.ERROR_USERS,
    payload: error
  };
};
