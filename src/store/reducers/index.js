import { combineReducers } from "redux";
import { getUsers, createUser, errorUser } from "./users";
import { loader } from "./loader";

const reducers = combineReducers({
  users: getUsers,
  user: createUser,
  loader,
  errorUser
});

export default reducers;
