import { getUsers, createUser } from "./users";
import * as actionTypes from "../actions/users";

describe("user reducers", () => {
  it("should return the initial state", () => {
    expect(getUsers(undefined, {})).toEqual([]);
  });

  it("should return the initial state", () => {
    expect(createUser(undefined, {})).toEqual(null);
  });

  it("should return the array of users", () => {
    expect(
      getUsers(undefined, {
        type: actionTypes.getUsersSuccess,
        payload: []
      })
    ).toEqual([]);
  });
});
