import * as actions from "../actions/loader";

const INITIAL_STATE = false;

export const loader = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case actions.Types.LOADER:
      return action.payload;
    default:
      return state;
  }
};
