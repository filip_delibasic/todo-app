import { loader } from "./loader";
import * as actionTypes from "../actions/loader";

describe("loader reducer", () => {
  it("should be initial state", () => {
    expect(loader(undefined, {})).toEqual(false);
  });

  it("should be boolean value", () => {
    expect(
      loader(Boolean, {
        type: actionTypes.loaderAction,
        payload: Boolean
      })
    ).toBe(Boolean);
  });
});
