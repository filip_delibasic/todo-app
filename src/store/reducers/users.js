import { Types } from "../actions/users";

const INITIAL_STATE = [];

export const getUsers = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Types.GET_USERS_SUCCESS:
      return action.payload;
    default:
      return state;
  }
};

export const createUser = (state = null, action) => {
  switch (action.type) {
    case Types.CREATE_USER_REQUEST:
      return action.payload;
    case Types.EDIT_USER_PUT_REQUEST:
      return action.payload;
    default:
      return state;
  }
};

export const errorUser = (state = "", action) => {
  switch (action.type) {
    case Types.ERROR_USERS:
      return action.payload;
    default:
      return state;
  }
};
