import React from "react";
import "./Input.scss";

const Input = props => {
  switch (props.element) {
    case "input":
      return (
        <div className="input-field">
          <input
            {...props}
            value={props.value}
            className={props.touch ? "touch" : null}
          />
          <span>
            <b>{props.errmessage}</b>
          </span>
        </div>
      );
    case "textarea":
      return (
        <div className="input-field">
          <textarea
            {...props}
            value={props.value}
            className={props.touch ? "touch" : null}
          />
        </div>
      );
    case "select":
      return (
        <div className="input-field-select">
          <select {...props} value={props.value}>
            {props.options
              ? props.options.map((item, index) => {
                  return (
                    <option value={index === 0 ? "" : item} key={item}>
                      {item}
                    </option>
                  );
                })
              : null}
          </select>
          <span>
            <b>{props.errmessage}</b>
          </span>
        </div>
      );
    default:
      return <input />;
  }
};

export default Input;
