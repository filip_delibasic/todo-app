import React from "react";
import "./ErrorHandler.scss";

const ErrorHandler = ({ error }) => {
  return <div className="error-handler">{error}</div>;
};

export default ErrorHandler;
