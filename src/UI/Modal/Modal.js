import React from "react";
import "./Modal.scss";

const Modal = ({ children, loader }) => {
  return <div className={loader ? "modal loader" : "modal"}>{children}</div>;
};

export default Modal;
