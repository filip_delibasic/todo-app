import React from "react";
import "./Button.scss";

const Button = ({ children, className, handleClick, disabled }) => {
  return (
    <div>
      <button className={className} onClick={handleClick} disabled={disabled}>
        {children}
      </button>
    </div>
  );
};

export default Button;
