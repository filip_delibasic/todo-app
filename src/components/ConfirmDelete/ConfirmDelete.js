import React from "react";
import Button from "../../UI/Button/Button";
import "./ConfirmDelete.scss";

const ConfirmDelete = ({ closeModal, deleteUser }) => {
  return (
    <div>
      <h2>Confirm Delete</h2>
      <p>Are you sure you want to delete this user?</p>
      <div className="btn-group-confirm">
        <Button className="accept-btn" handleClick={deleteUser}>
          Yes
        </Button>
        <Button className="cancel-btn" handleClick={closeModal}>
          No
        </Button>
      </div>
    </div>
  );
};

export default ConfirmDelete;
