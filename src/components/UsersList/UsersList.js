import React from "react";
import "./UsersList.scss";
import Button from "../../UI/Button/Button";

const UsersList = ({ users, editUser, deleteModal }) => {
  let list = <div>List is empty</div>;
  if (users.length) {
    list = users.map(user => {
      return (
        <li key={user.id}>
          <div className="user-name">
            {user.firstName.firstName} {user.lastName.lastName}
          </div>
          <div className="btn-group">
            <Button
              className="btn btn-edit"
              handleClick={() => editUser(user.id)}
            >
              <i className="material-icons">edit</i>
            </Button>
            <Button
              className="btn btn-delete"
              handleClick={() => deleteModal(user.id)}
            >
              <i className="material-icons">delete</i>
            </Button>
          </div>
        </li>
      );
    });
  }
  return (
    <div className="users-list">
      Users:
      <ul>{list}</ul>
    </div>
  );
};

export default UsersList;
